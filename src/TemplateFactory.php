<?php

namespace AJAXimple\Template;

use Nette\Bridges\ApplicationLatte\Template;
use Nette\Application\UI\Control;
use Nette\Application\UI\ITemplate;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Http\IRequest;
use Nette\Security\User;
use Nette\Caching\IStorage;

class TemplateFactory extends \Nette\Bridges\ApplicationLatte\TemplateFactory
{
    /**
     * @var string Absolute path to www dir
     */
    private $wwwDir;
    
    public function __construct(
            string $wwwDir,
            ILatteFactory $latteFactory, 
            IRequest $httpRequest = null, 
            User $user = null, 
            IStorage $cacheStorage = null, 
            $templateClass = null) {
        parent::__construct($latteFactory, $httpRequest, $user, $cacheStorage, $templateClass);
        $this->wwwDir = $wwwDir;
    }
    
    public function createTemplate(Control $control = null): ITemplate 
    {
        /** @var Template $template */
        $template = parent::createTemplate($control);
        
        $template->addFilter('loadFile', [$this, 'filterLoadFile']);
        
        return $template;
    }
    
    /**
     * Add created date url parameter
     * 
     * @param string $filePath Path to the file located in www folder
     * 
     * @return string $filePath with ?created parameter
     */
    public function filterLoadFile(string $filePath): string
    {
        if (!preg_match("/^\//", $filePath)) {
            $filePath = "/" . $filePath;
        }
        
        $fullPath = $this->wwwDir . $filePath;
        if (file_exists($fullPath)) {
            return $filePath . "?modified=" . filemtime($fullPath);
        } else {
            return $filePath;
        }
    }
}