# Quickstart
This extension extends Nette\Bridges\ApplicationLatte\TemplateFactory for new filters.

## Added filters
### loadFile
```sh
<link href="{$basePath}{('/css/card.css')|loadFile}" rel="stylesheet">
```
add modified parameter after file name located in www dir, so after each changes will be this file downloaded by browser.

## Instalation
#### Download
The best way to install `AJAXimple/Template` is using Composer:
```sh
$ composer require ajaximple/template
```
#### Registering
You can enable the extension using your neon config:
```sh
services:
	latte.templateFactory: AJAXimple\Template\TemplateFactory(wwwDir: %wwwDir%) # wwwDir is path to the dir with yours public files on your harddrive
```

## Conclusion
This extension requires Nette3.0 and it is property of Antonín Jehlář © 2020